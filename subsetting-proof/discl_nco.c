#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <pnetcdf.h>
#include <string.h>

#include "discl_netcdf.h"
#include "discl_bo.h"

int rank;				// The rank of the current process
int process_count;			// The number of processes in this MPI session

char *class;				// The class of the operation
char *operation;			// The name of the operation to perform
int input_file_count;			// The number of input files the user specified

int output_flag = 0;

NetCDFMetadata *input_file;		// An array containing the information about each input file
NetCDFMetadata output_file;		// An array containing the information about the output file

double time_start, time_end;

void main (int argc, char **argv) {
	int i, j, k;					// Loop counter								
	
	MPI_Init(&argc, &argv);				// Initialize MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);		// Get the rank of the current process
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);	// Get the number of processes in the this MPI session
	MPI_Barrier(MPI_COMM_WORLD);
	
	if (rank == 0)
		time_start = MPI_Wtime();
	
	class			= argv[1];		// The class of the operation
	operation 		= argv[2];		// The name of the operation
	
	if (strcmp(class, "bo") == 0) {
		if (argc < 4) {
			printf("Correct usage: mpirun -np <number of processes> ./discl_nco bo <operation> <input file 1> <input file 2> ... <input file n> <output file>\n");
			MPI_Finalize();
			exit(-1);
		}
		
		input_file_count	= argc - 4;		// The number of input files the user specified
		output_file.path 	= argv[argc - 1];	// The path to the output file
		
		output_flag = 1;
	} else if (strcmp(class, "ens") == 0) {
		if (argc < 3) {
			printf("Correct usage: mpirun -np <number of processes> ./discl_nco ens <operation> <input file 1> <input file 2> ... <input file n> \n");
			MPI_Finalize();
			exit(-1);
		}
		
		input_file_count 	= argc - 3;
	} else {
		printf("Invalid class specified: %s\n", class);
		exit(-1);
	}

	
	// ------------------------ FILE METADATA ---------------------
	input_file = malloc(input_file_count * sizeof(NetCDFMetadata));
	for (i = 0; i < input_file_count; i++) {
		input_file[i].path = argv[i + 3];

		// Open the file
		try_ncmpi(ncmpi_open(MPI_COMM_WORLD, input_file[i].path, NC_NOWRITE, MPI_INFO_NULL, &input_file[i].id), __LINE__);
		
		// Store general information about the file
		try_ncmpi(ncmpi_inq(input_file[i].id, &input_file[i].ndims, &input_file[i].nvars, &input_file[i].ngatts, &input_file[i].unlimited), __LINE__);
		
		// Store information about each dimension of the file
		input_file[i].dimension = malloc(input_file[i].ndims * sizeof(NetCDFDimension));
		for (j = 0; j < input_file[i].ndims; j++) {
			input_file[i].dimension[j].id = j;
			try_ncmpi(ncmpi_inq_dim(input_file[i].id, j, input_file[i].dimension[j].name, &input_file[i].dimension[j].size), __LINE__);
		}
			
		// Store information about each variable of the file
		input_file[i].variable = malloc(input_file[i].nvars * sizeof(NetCDFVariable));
		for (j = 0; j < input_file[i].nvars; j++) {
			input_file[i].variable[j].id = j;
			try_ncmpi(ncmpi_inq_var(input_file[i].id, j, input_file[i].variable[j].name, &input_file[i].variable[j].type, &input_file[i].variable[j].ndims, input_file[i].variable[j].dimids, &input_file[i].variable[j].natts), __LINE__);
		}
	}
	
	// --------------------- SETUP THE OUTPUT FILE ---------------------
	if (output_flag == 1)
		try_ncmpi(ncmpi_create(MPI_COMM_WORLD, output_file.path, NC_CLOBBER|NC_64BIT_OFFSET, MPI_INFO_NULL, &output_file.id), __LINE__);	// Create output file
	
	if (strcmp(class, "bo") == 0)
		binary_operation(operation, input_file, input_file_count, output_file, rank, process_count);
	else if (strcmp(class, "ens") == 0)
		ensemble(operation, input_file, input_file_count, rank, process_count);
	
	finalize(input_file, input_file_count);	// close all files and finalize the MPI session
	
	if (output_flag == 1)
		finalize_output(output_file);
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	if (rank == 0) {
		time_end = MPI_Wtime();
		printf("Completed successfully on %d processes. Time elapsed: %f seconds.\n", process_count, time_end - time_start); 
	}
	MPI_Finalize();
}
