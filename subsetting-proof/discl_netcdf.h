#include <pnetcdf.h>

typedef struct NetCDFDimension {
	MPI_Offset size;
	char name[NC_MAX_NAME+1];
	int id;
} NetCDFDimension;

typedef struct NetCDFVariable {
	char name[NC_MAX_NAME+1];
	nc_type type;
	int ndims;
	int dimids[NC_MAX_VAR_DIMS];
	int natts;
	int id;
} NetCDFVariable;

typedef struct NetCDFMetadata {
	char *path;
	
	int id;
	int ndims;
	int nvars;
	int ngatts;
	int unlimited;
	
	NetCDFDimension *dimension;
	NetCDFVariable *variable;
} NetCDFMetadata;

void try_ncmpi(int return_id, int line_number);
void finalize(NetCDFMetadata *input_file, int input_file_count);
void finalize_output(NetCDFMetadata output_file);
void setup_output_file(NetCDFMetadata output_file);