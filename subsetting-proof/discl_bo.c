#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "discl_netcdf.h"

void bo_maximum(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_max = input[0][i];						// The starting maximum value should be the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files, checking the corresponding values in each file
			current_max = current_max > input[j][i] ? current_max : input[j][i];	// If the value in the new file is higher than the current max, store the new value
		output[i] = current_max;							// Store the output
	}
}

void bo_average(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_sum = input[0][i];
		for (j = 1; j < input_file_count; j++)
			current_sum += input[j][i];
		output[i] = current_sum / input_file_count;
	}
}

void bo_minimum(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_min = input[0][i];						// The starting minimum value should be the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files, checking the corresponding values in each file
			current_min = current_min < input[j][i] ? current_min : input[j][i];	// If the value in the new file is lower than the current min, store the new value
		output[i] = current_min;							// Store the output
	}
}

void bo_multiplication(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_product = input[0][i];						// The starting product is equal to the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files
			current_product *= input[j][i];						// Multiply the current product by the value in the new file
		output[i] = current_product;							// Store the output
	}
}

void bo_division(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_quotient = input[0][i];						// The starting quotient is equal to the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files
			current_quotient /= input[j][i];					// Divide the current quotient by the value in the new file
		output[i] = current_quotient;							// Store the output
	}
}

void bo_addition(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_sum = input[0][i];						// The starting sum is equal to the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files
			current_sum += input[j][i];						// Add the new value to the current sum
		output[i] = current_sum;							// Store the output
	}
}

void bo_subtraction(float **input, int input_file_count, float *output, int output_size) {
	int i, j;
	
	for (i = 0; i < output_size; i++) {
		float current_difference = input[0][i];						// The starting difference is equal to the value in the first file
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files
			current_difference -= input[j][i];					// Subtract the new value from the current difference
		output[i] = current_difference;							// Store the output
	}
}

void binary_operation(char *operation, NetCDFMetadata *input_file, int input_file_count, NetCDFMetadata output_file, int rank, int process_count) {
	int i, j, k;
	
	output_file.ndims = input_file[0].ndims;					// The number of output dimensions is equal to the number of dimensions in the first input file
	output_file.nvars = input_file[0].nvars;					// The number of output variables is equal to the number of variables in the first input file

	output_file.dimension = malloc(output_file.ndims * sizeof(NetCDFDimension));	// Allocate space for the dimensions
	output_file.variable  = malloc(output_file.nvars * sizeof(NetCDFVariable));	// Allocate space for the variables
	
	for (i = 0; i < output_file.ndims; i++)						// Iterate through the dimensions in the input file
		output_file.dimension[i] = input_file[0].dimension[i];			// Copy dimension data from the input file to be used in the output file
	
	for (i = 0; i < output_file.nvars; i++)						// Iterate through the variables in the input file
		output_file.variable[i] = input_file[0].variable[i];			// Copy variable data from the input file to be used in the output file
	
	setup_output_file(output_file);							// Setup the data in the output file

	for (i = 0; i < output_file.nvars; i++) {					// Iterate through the variables in the output file
		MPI_Offset *start, *count;
		int local_size;
		
		start = malloc(output_file.variable[i].ndims * sizeof(MPI_Offset));
		count = malloc(output_file.variable[i].ndims * sizeof(MPI_Offset));
		
		count[0] = (output_file.dimension[output_file.variable[i].dimids[0]].size) / process_count;	// The number of values on the first dimension
		start[0] = count[0] * rank;									// The starting position for this dimension
		
		// If this is the last section, account for the values that could have been left
		// off due to rounding errors. Example: If there are 50 values and 12 processes
		// then each process will be allocated 4 values (4 * 12 = 48). This would normally
		// leave off the last two values, so they will be taken by the last process
		if (rank == process_count - 1) {
			int missing_entries = output_file.dimension[output_file.variable[i].dimids[0]].size - (count[0] * process_count);
			count[0] += missing_entries;
		}
		local_size = count[0];
		
		for (j = 1; j < output_file.variable[i].ndims; j++) {					// Iterate through the other dimensions in this variable
			start[j] = 0;									// Start at the first value
			count[j] = output_file.dimension[output_file.variable[i].dimids[j]].size;	// End at the last value
			
			local_size *= count[j];								// Store the running total of the size of this processes work
		}
		
		float **data, *output;
		
		data = malloc(input_file_count * sizeof(float *));
		output = malloc(local_size * sizeof(float));

		for (j = 0; j < input_file_count; j++) {			// Iterate through the input files and get the data for this processes section
				data[j] = malloc(local_size * sizeof(float));
				try_ncmpi(ncmpi_get_vara_all(input_file[j].id, i, start, count, data[j], local_size, MPI_FLOAT), __LINE__);
		}
				
		// Perform the operation
			
		if (strcmp(operation, "max") == 0)
			bo_maximum(data, input_file_count, output, local_size);
		else if (strcmp(operation, "min") == 0)
			bo_minimum(data, input_file_count, output, local_size);
		else if (strcmp(operation, "sum") == 0)
			bo_addition(data, input_file_count, output, local_size);
		else if (strcmp(operation, "sub") == 0)
			bo_subtraction(data, input_file_count, output, local_size);
		else if (strcmp(operation, "mul") == 0)
			bo_multiplication(data, input_file_count, output, local_size);
	    else if (strcmp(operation, "div") == 0)
			bo_division(data, input_file_count, output, local_size);
		else if (strcmp(operation, "avg") == 0)
			bo_average(data, input_file_count, output, local_size);
		else
			printf("Unknown Operation: %s\n", operation);
			
		try_ncmpi(ncmpi_put_vara_all(output_file.id, i, start, count, output, local_size, MPI_FLOAT), __LINE__);	// Store the values
	
		free(start);
		free(count);
	}
}
