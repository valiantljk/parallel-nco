#include <stdio.h>
#include <mpi.h>

#include "discl_netcdf.h"

void try_ncmpi(int return_id, int line_number) {
	if (return_id != NC_NOERR) {
		printf("Error encountered at line %d: %s\n", line_number, ncmpi_strerror(return_id));
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
}

void finalize(NetCDFMetadata *input_file, int input_file_count) {
	int i;
	
	for (i = 0; i < input_file_count; i++)
		try_ncmpi(ncmpi_close(input_file[i].id), __LINE__);		
}

void finalize_output(NetCDFMetadata output_file) {
	try_ncmpi(ncmpi_close(output_file.id), __LINE__);
}

void setup_output_file(NetCDFMetadata output_file) {
	int i;
	for (i = 0; i < output_file.ndims; i++)
		try_ncmpi(ncmpi_def_dim(output_file.id, output_file.dimension[i].name, output_file.dimension[i].size, &output_file.dimension[i].id), __LINE__);	
	for (i = 0; i < output_file.nvars; i++)
		try_ncmpi(ncmpi_def_var(output_file.id, output_file.variable[i].name, output_file.variable[i].type, output_file.variable[i].ndims, output_file.variable[i].dimids, &output_file.variable[i].id), __LINE__);
	
	try_ncmpi(ncmpi_enddef(output_file.id), __LINE__);
}
