#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "discl_netcdf.h"

float ens_maximum(float **input, int input_file_count, int input_length) {
	int i, j;
	
	float local_max = input[0][i];
	for (i = 0; i < input_length; i++) {
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files, checking the corresponding values in each file
			local_max = local_max > input[j][i] ? local_max : input[j][i];		// If the value in the new file is higher than the current max, store the new value
	}
	return local_max;
}

float ens_minimum(float **input, int input_file_count, int input_length) {
	int i, j;
	
	float local_min = input[0][i];
	for (i = 0; i < input_length; i++) {
		for (j = 1; j < input_file_count; j++)						// Iterate through the input files, checking the corresponding values in each file
			local_min = local_min < input[j][i] ? local_min : input[j][i];		// If the value in the new file is lower than the current min, store the new value
	}
	return local_min;
}

void ensemble(char *operation, NetCDFMetadata *input_file, int input_file_count, int rank, int process_count) {
	int i, j;

	for (i = 0; i < input_file[0].nvars; i++) {					// Iterate through the variables in the output file
		MPI_Offset *start, *count;
		int local_size;
		
		start = malloc(input_file[0].variable[i].ndims * sizeof(MPI_Offset));
		count = malloc(input_file[0].variable[i].ndims * sizeof(MPI_Offset));
		
		count[0] = (input_file[0].dimension[input_file[0].variable[i].dimids[0]].size) / process_count;	// The number of values on the first dimension
		start[0] = count[0] * rank;									// The starting position for this dimension
		
		// If this is the last section, account for the values that could have been left
		// off due to rounding errors. Example: If there are 50 values and 12 processes
		// then each process will be allocated 4 values (4 * 12 = 48). This would normally
		// leave off the last two values, so they will be taken by the last process
		if (rank == process_count - 1) {
			int missing_entries = input_file[0].dimension[input_file[0].variable[i].dimids[0]].size - (count[0] * process_count);
			count[0] += missing_entries;
		}
		local_size = count[0];
		
		for (j = 1; j < input_file[0].variable[i].ndims; j++) {					// Iterate through the other dimensions in this variable
			start[j] = 0;									// Start at the first value
			count[j] = input_file[0].dimension[input_file[0].variable[i].dimids[j]].size;	// End at the last value
			
			local_size *= count[j];								// Store the running total of the size of this processes work
		}
		
		float **data;
		float local_output;
		float output;
		
		data = malloc(input_file_count * sizeof(float *));

		for (j = 0; j < input_file_count; j++) {			// Iterate through the input files and get the data for this processes section
			data[j] = malloc(local_size * sizeof(float));
			try_ncmpi(ncmpi_get_vara_all(input_file[j].id, i, start, count, data[j], local_size, MPI_FLOAT), __LINE__);
		}
				
		// Perform the operation
		
		if (strcmp(operation, "max") == 0) {
			local_output = ens_maximum(data, input_file_count, local_size);
			MPI_Barrier(MPI_COMM_WORLD);											// Wait for all processes to finish this variable
			MPI_Reduce(&local_output, &output, 1, MPI_FLOAT, MPI_MAX, 0, MPI_COMM_WORLD);
		} else if (strcmp(operation, "min") == 0) {
			local_output = ens_maximum(data, input_file_count, local_size);
			MPI_Barrier(MPI_COMM_WORLD);											// Wait for all processes to finish this variable
			MPI_Reduce(&local_output, &output, 1, MPI_FLOAT, MPI_MIN, 0, MPI_COMM_WORLD);
		} else
			printf("Unknown Operation: %s\n", operation);
			
		if (rank == 0)
			printf("%s: %f\n", input_file[0].variable[i].name, output);
	
		free(start);
		free(count);
	}
}
